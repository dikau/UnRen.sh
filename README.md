# UnRen.sh

Bash script for unpacking and decompiling games made with the [Ren'Py](https://renpy.org) visual novel engine.
Port of Windows script [UnRen-ultrahack(vXX).bat](https://github.com/VepsrP/UnRen-Gideon-mod-/releases) by [VepsrP](https://github.com/VepsrP) for Linux or macOS

### Usage:
```bash
./UnRen.sh [OPTIONAL-path-to-games-main-folder-or-bundle-app]
```
Download from [Release](https://codeberg.org/dikau/UnRen.sh/releases/latest).